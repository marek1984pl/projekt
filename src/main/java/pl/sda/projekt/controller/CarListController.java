/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.projekt.controller;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import pl.sda.projekt.Model.Car;

/**
 *
 * @author marecki
 */
@ManagedBean (name = "carListController")
@RequestScoped
public class CarListController {

    @ManagedProperty (value="#{dbController}")
    private DbController dbController;
    
    public CarListController() {
    }
    
    public List<Car> getPostsList() {
        EntityManager em = dbController.getEntityManager();
        Query query = em.createQuery("from Car c");
        return query.getResultList();
    }

    public DbController getDbController() {
        return dbController;
    }

    public void setDbController(DbController dbController) {
        this.dbController = dbController;
    }
    
    public Car showCarDetails(long carId) {
        EntityManager em = dbController.getEntityManager();
        return em.find(Car.class, carId);
    }
    
    public void removeCar(Car car) {
        EntityManager em = dbController.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        em.remove(car);
        tx.commit();
    }
}
