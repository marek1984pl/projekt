/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.projekt.controller;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Marek
 */
@ManagedBean(name = "dbController")
@SessionScoped
public class DbController implements Serializable {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public DbController() {
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();
    }

    public EntityManager getEntityManager() {
        return em;
    }
}
