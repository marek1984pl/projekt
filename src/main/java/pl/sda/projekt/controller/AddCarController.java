/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.projekt.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import pl.sda.projekt.Model.Car;

/**
 *
 * @author marecki
 */
@ManagedBean(name = "addCarController")
@RequestScoped
public class AddCarController {

    private Car car = new Car();
    
    @ManagedProperty(value = "#{dbController}")
    private DbController dbController;
    
    public AddCarController() {
    }
    
    public void addCar() {
        //todo walidacja
        EntityManager em = dbController.getEntityManager();
        EntityTransaction tx = em.getTransaction();

        tx.begin();
        em.persist(this.car);
        tx.commit();
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public DbController getDbController() {
        return dbController;
    }

    public void setDbController(DbController dbController) {
        this.dbController = dbController;
    }
}
